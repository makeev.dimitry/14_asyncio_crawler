import asyncio
import json
import logging
import os
import re
from hashlib import md5
from optparse import OptionParser
from typing import Union

import aiofiles as aiofiles
import aiohttp
from bs4 import BeautifulSoup

URL = 'https://news.ycombinator.com'
OUTPUT_DIRECTORY = 'pages'
INTERVAL = 60


async def get_page(url: str, return_bytes: bool = False) -> Union[str, bytes]:
    try:
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as response:
                if return_bytes:
                    return await response.read()
                else:
                    return await response.text()
    except Exception as exc:
        logging.error(str(exc))


async def parse_comments(url: str, path: str) -> None:
    logging.debug('Parse comments %s' % url)

    page = BeautifulSoup(await get_page(url), 'html.parser')
    links = []
    for comment in page.find_all(class_=re.compile('commtext')):
        links += [*comment.find_all('a', href=re.compile('http'))]
    href_list = [link.get('href') for link in links]

    if len(href_list) > 0:
        if not os.path.exists(path):
            os.makedirs(path)

        href_dict = {md5(href.encode()).hexdigest(): href for href in href_list}
        async with aiofiles.open(os.path.join(path, 'links.json'), 'w+') as file:
            await file.write(json.dumps(href_dict))

        tasks = []
        for key, url in href_dict.items():
            tasks.append(asyncio.create_task(
                save_page(url, os.path.join(path, key))
            ))
        await asyncio.gather(*tasks)


async def save_page(url: str, filepath: str) -> bool:
    if page := await get_page(url, return_bytes=True):
        async with aiofiles.open(filepath, 'wb+') as file:
            await file.write(page)
        return True
    return False


async def parse_page(url: str, filepath: str, comments_url: str, comments_path: str) -> None:
    logging.debug('Download page %s' % url)
    if await save_page(url, filepath):
        await parse_comments(comments_url, comments_path)


def format_url(hostname: str, href: str) -> str:
    return href if href.startswith('http') else f'{hostname}/{href}'


async def parse_homepage(config) -> None:
    parse = BeautifulSoup(await get_page(config.url), 'html.parser')

    tasks = []
    news_ids = [tr.get('id') for tr in parse('tr', class_='athing')]
    for news_id in news_ids:
        filepath = os.path.join(config.directory, news_id)
        if not os.path.exists(filepath):
            news_link = parse.find('tr', id=news_id).find('a', class_='titlelink')
            comments_href = parse.find('tr', id=news_id).next_sibling('a')[-1].get('href')

            logging.info('Download news #%s: %s' % (news_id, news_link.text))

            news_url = format_url(config.url, news_link.get('href'))
            comments_path = os.path.join(config.directory, 'comments', news_id)
            comments_url = format_url(config.url, comments_href)
            tasks.append(asyncio.create_task(
                parse_page(news_url, filepath, comments_url, comments_path)
            ))

    if len(tasks) > 0:
        await asyncio.gather(*tasks)


async def main(config) -> None:
    while True:
        await parse_homepage(config)
        await asyncio.sleep(config.interval)


if __name__ == '__main__':
    op = OptionParser()
    op.add_option("-u", "--url", action="store", default=URL)
    op.add_option("-i", "--interval", action="store", default=INTERVAL)
    op.add_option("-d", "--directory", action="store", default=OUTPUT_DIRECTORY)

    (opts, args) = op.parse_args()
    logging.basicConfig(filename=None, level=logging.INFO,
                        format='[%(asctime)s] %(levelname).1s %(message)s',
                        datefmt='%Y.%m.%d %H:%M:%S')

    logging.info('Crawler started')

    if not os.path.exists(opts.directory):
        raise RuntimeError(f'Directory "{opts.directory}" does not exists')

    asyncio.run(main(opts))
